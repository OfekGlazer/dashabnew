import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    Soldiers : []
  },
  mutations: {
    setSoldiers (state, soldiers) {
      state.Soldiers = soldiers
    }
  },
  getters: {
    getSoldiers: state => {
      return state.Soldiers
    }
  },
  actions: {
  },
  modules: {
  }
})
