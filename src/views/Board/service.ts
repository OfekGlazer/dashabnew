import { Movement, MovementLine, MovementOnScreen, MovementValue, Soldier } from "@/Models/Board";

export const mockSoldiers: Soldier[] = [
    {
        age: 12,
        name: "ofek glazer",
        pernr: "1241245",
        movements: [
            {
                Continue: MovementValue.None,
                KidumBreadth: MovementValue.None,
                Quit: MovementValue.None,
                Discharged: MovementValue.None,
                Breadth: MovementValue.Primary,
                Vacation: MovementValue.Secondary,
            }
        ]

    },
    {
        age: 12,
        name: "ofek glazer",
        pernr: "7657554",
        movements: [
            {
                Continue: MovementValue.Secondary,
                KidumBreadth: MovementValue.None,
                Quit: MovementValue.Secondary,
                Discharged: MovementValue.None,
                Breadth: MovementValue.Primary,
                Vacation: MovementValue.None,
            }
        ]

    },
    {
        age: 12,
        name: "ofek glazer",
        pernr: "6366544",
        movements: [
            {
                Continue: MovementValue.None,
                KidumBreadth: MovementValue.Primary,
                Quit: MovementValue.None,
                Discharged: MovementValue.None,
                Breadth: MovementValue.Secondary,
                Vacation: MovementValue.Secondary,
            }
        ]

    },
    {
        age: 12,
        name: "ofek glazer",
        pernr: "52352324",
        movements: [
            {
                Continue: MovementValue.Primary,
                KidumBreadth: MovementValue.None,
                Quit: MovementValue.None,
                Discharged: MovementValue.None,
                Breadth: MovementValue.Secondary,
                Vacation: MovementValue.None,
            }
        ]

    },
    {
        age: 12,
        name: "ofek glazer",
        pernr: "5235432",
        movements: [
            {
                Continue: MovementValue.None,
                KidumBreadth: MovementValue.Primary,
                Quit: MovementValue.None,
                Discharged: MovementValue.None,
                Breadth: MovementValue.None,
                Vacation: MovementValue.None,
            }
        ]

    },
]

export const getPrimaryMovement = (movement: Movement): string | undefined => {
    // debugger;
    let prop: string | undefined = undefined
    Object.keys(movement).forEach(movementProp => {
        if ((movement as any)[movementProp] == MovementValue.Primary) {
            prop = movementProp
        }
    })
    return prop
}

export const getMovement = (movement: Movement, type: MovementValue): string[] => {
    // debugger;
    const props: string[] = []
    Object.keys(movement).forEach(movementProp => {
        if ((movement as any)[movementProp] == type) {
            props.push(movementProp)
        }
    })
    return props
}

export const mapSoldierMovementsToScreen = (movement: Movement, isDisabled: boolean): MovementLine => {

    // debugger;

    const movementLine: MovementLine = {
        primaryMovement: {
            disabled: isDisabled,
            id: '',
            isPrimary: true,
            text: ''
        },
        firstMovement: {
            disabled: isDisabled,
            id: '',
            isPrimary: false,
            text: ''
        },
        secondMovement: {
            disabled: isDisabled,
            id: '',
            isPrimary: false,
            text: ''
        }
    }

    let currSearchMovement: string[] = getMovement(movement, MovementValue.Primary)
    if (currSearchMovement.length == 1) {
        movementLine.primaryMovement.id = currSearchMovement[0]
        movementLine.primaryMovement.text = getMovementTextById(currSearchMovement[0])
    }

    currSearchMovement = getMovement(movement, MovementValue.Secondary)
    if (currSearchMovement.length >= 1) {
        movementLine.firstMovement.id = currSearchMovement[0]
        movementLine.firstMovement.text = getMovementTextById(currSearchMovement[0])
    }

    if (currSearchMovement.length == 2) {
        movementLine.secondMovement.id = currSearchMovement[1]
        movementLine.secondMovement.text = getMovementTextById(currSearchMovement[1])
    }

    return movementLine

    // let currentMovement: MovementOnScreen;
    // let primaryMovements: MovementOnScreen[];
    // let secondaryMovements: MovementOnScreen[];

    // Object.keys(movement).forEach(movementProp => {

    //     currentMovement = {
    //         disabled: isDisabled,
    //         id: movementProp,
    //         isPrimary: false,
    //         text: (movementTexts.find(mov => mov.id == movementProp) as any).text,
    //     }
    //     if ((movement as any)[movementProp] == MovementValue.Primary) {
    //         currentMovement.isPrimary = true
    //     } else if ((movement as any)[movementProp] == MovementValue.Secondary) {
    //         currentMovement.isPrimary = false
    //     }
    // })

}

export const getMovementTextById = (id: string): string => {
    return (movementTexts.find(mov => mov.id == id) as any).text
}

export const movementTexts = [
    {
        id: "KidumBreadth",
        text: "ק",
    },
    {
        id: "Breadth",
        text: "ר",
    },
    {
        id: "Continue",
        text: "מ",
    },
    {
        id: "Vacation",
        text: "ח",
    },
    // {
    //     id: "studies",
    //     text: "ל",
    // },
    // {
    //     id: "kidum",
    //     text: "ק",
    // },
    {
        id: "Discharged",
        text: "ש",
    },
    {
        id: "Quit",
        text: "פ",
    },
]