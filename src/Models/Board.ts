export interface Soldier {
    pernr : string
    name : string
    age : number
    movements : Movement[]
}


export interface Movement {
    KidumBreadth : MovementValue,
    Breadth : MovementValue,
    Continue : MovementValue,
    Vacation : MovementValue,
    // studies : MovementValue,
    Discharged : MovementValue,
    Quit : MovementValue
}

export interface MovementLine {
    primaryMovement: MovementOnScreen,
    firstMovement: MovementOnScreen,
    secondMovement: MovementOnScreen,
}



export enum MovementValue {
    Primary = "2",
    Secondary = "1",
    None = "0"
}
export enum MovementCubeValue {
    Kidum = "ק", 
}

export enum MovementIds {
    kidum = "kidum"
}

export interface MovementOnScreen {
    id: string;
    text: string;
    disabled: boolean;
    isPrimary: boolean;
  }